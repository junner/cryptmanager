//
//  main.m
//  CryptManager
//
//  Created by qijunxi on 10/23/2023.
//  Copyright (c) 2023 qijunxi. All rights reserved.
//

@import UIKit;
#import "JXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JXAppDelegate class]));
    }
}
