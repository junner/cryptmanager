# CryptManager

[![CI Status](https://img.shields.io/travis/qijunxi/CryptManager.svg?style=flat)](https://travis-ci.org/qijunxi/CryptManager)
[![Version](https://img.shields.io/cocoapods/v/CryptManager.svg?style=flat)](https://cocoapods.org/pods/CryptManager)
[![License](https://img.shields.io/cocoapods/l/CryptManager.svg?style=flat)](https://cocoapods.org/pods/CryptManager)
[![Platform](https://img.shields.io/cocoapods/p/CryptManager.svg?style=flat)](https://cocoapods.org/pods/CryptManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CryptManager is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CryptManager'
```

## Author

qijunxi, 156718768@qq.com

## License

CryptManager is available under the MIT license. See the LICENSE file for more info.
