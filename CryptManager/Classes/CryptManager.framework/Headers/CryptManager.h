//
//  CryptManager.h
//  CryptManager
//
//  Created by 祁俊喜 on 2022/5/24.
//

/**
 错误码
 1000  密文错误
 1001 公钥错误
 1002 验签失败
 1003 解密失败
 1004 bundleId不匹配
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface CryptManager : NSObject

/**
 * RSA解密
 * @param text 密文
 * @param pubKey 公钥
 * @param error 错误码
 */
+ (BOOL)decryptRSAString:(NSString *)text publicKey:(NSString *)pubKey error:(NSError **)error;

/**
 * 获取版本号
 */
+ (NSString *)getVersion;

@end

NS_ASSUME_NONNULL_END
